import pytest
import json
from rest_framework.reverse import reverse


pytestmark = [pytest.mark.unit, pytest.mark.django_db]


DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'


def test_list_competition_endpoint(api_client, competition_factory):
    competition_factory.create_batch(5)

    endpoint = reverse("competition-list")
    response = api_client.get(endpoint)
    parsed_json = json.loads(response.content)

    assert response.status_code == 200
    assert len(parsed_json["results"]) == 5


def test_retrieve_competition_endpoint(api_client, get_image_path, competition_factory):
    competition = competition_factory.create()
    expected_json = {
        "id": competition.id,
        "name": competition.name,
        "date": competition.date.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
        "description": competition.description,
        "location": competition.location,
        "vip_ticket_price": str(competition.vip_ticket_price),
        "regular_ticket_price": str(competition.regular_ticket_price),
        "images": [
            {"id": image.id, "image": get_image_path(image.image)}
            for image in competition.images.all()
        ],
        "competition_categories": [
            {"id": category.id, "name": category.name, "price": str(category.price)}
            for category in competition.competition_categories.all()
        ],
        "sponsors": [
            {
                "id": sponsor.id,
                "name": sponsor.name,
                "logo": get_image_path(sponsor.logo),
                "description": sponsor.description,
            }
            for sponsor in competition.sponsors.all()
        ],
    }

    endpoint = reverse("competition-detail", kwargs={"pk": competition.id})
    response = api_client.get(endpoint)
    parsed_json = json.loads(response.content)

    assert response.status_code == 200
    assert parsed_json == expected_json
