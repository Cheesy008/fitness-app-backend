from rest_framework.reverse import reverse


def test_healthceck_endpoint(api_client):
    endpoint = reverse("health-check")
    response = api_client.get(endpoint)

    response.status_code == 200
