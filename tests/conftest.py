import pytest
from pytest_factoryboy import register

from rest_framework.test import APIClient

from .test_users.factories import UserFactory
from .test_catalogs.factories import GalleryCategoryFactory, SponsorFactory
from .test_competitions.factories import CompetitionFactory


register(UserFactory)
register(GalleryCategoryFactory)
register(SponsorFactory)
register(CompetitionFactory)


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def test_user(db, user_factory):
    return user_factory.create()


@pytest.fixture
def api_client_with_credentials(db, test_user):
    client = APIClient()
    client.force_authenticate(user=test_user)
    return client


@pytest.fixture
def custom_api_client_with_credentials(db):
    """
    api_client, в который можно передать
    кастомного пользователя.
    """

    def api_client(user):
        client = APIClient()
        client.force_authenticate(user=user)
        return client

    return api_client


@pytest.fixture
def get_image_path():
    def inner(image):
        return f"http://testserver/{image.path[6:]}"

    return inner


@pytest.fixture(autouse=True)
def redefine_throttling_frequency(settings):
    settings.REST_FRAMEWORK = {
        "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.AllowAny"],
        "DEFAULT_AUTHENTICATION_CLASSES": [
            "rest_framework_simplejwt.authentication.JWTAuthentication",
        ],
        "DEFAULT_THROTTLE_RATES": {
            "send_email": "100/min",
        },
        "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
        "PAGE_SIZE": 10,
        "EXCEPTION_HANDLER": "apps.core.utils.custom_exception_handler",
    }
