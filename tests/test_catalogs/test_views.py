import pytest
import json
from rest_framework.reverse import reverse


pytestmark = [pytest.mark.unit, pytest.mark.django_db]


class TestGalleryEndpoints:
    def test_list_endpoint(self, api_client, gallery_category_factory):
        gallery_category_factory.create_batch(size=5)

        endpoint = reverse("gallery-list")
        response = api_client.get(endpoint)
        parsed_json = json.loads(response.content)

        assert response.status_code == 200
        assert len(parsed_json["results"]) == 5

    def test_retrieve_endpoint(
        self, api_client, get_image_path, gallery_category_factory
    ):
        gallery = gallery_category_factory.create()
        expected_json = {
            "id": gallery.id,
            "name": gallery.name,
            "images": [
                {"id": item.id, "image": get_image_path(item.image)}
                for item in gallery.images.all()
            ],
        }

        endpoint = reverse("gallery-detail", kwargs={"pk": gallery.id})
        response = api_client.get(endpoint)
        parsed_json = json.loads(response.content)

        assert response.status_code == 200
        assert parsed_json == expected_json


class TestSponsorEndpoints:
    def test_list_endpoint(self, api_client, sponsor_factory):
        sponsor_factory.create_batch(size=5)

        endpoint = reverse("sponsor-list")
        response = api_client.get(endpoint)
        parsed_json = json.loads(response.content)

        assert response.status_code == 200
        assert len(parsed_json["results"]) == 5

    def test_retrieve_endpoint(self, api_client, get_image_path, sponsor_factory):
        sponsor = sponsor_factory.create()
        expected_json = {
            "id": sponsor.id,
            "name": sponsor.name,
            "logo": get_image_path(sponsor.logo),
            "description": sponsor.description,
        }

        endpoint = reverse("sponsor-detail", kwargs={"pk": sponsor.id})
        response = api_client.get(endpoint)
        parsed_json = json.loads(response.content)

        assert response.status_code == 200
        assert parsed_json == expected_json
