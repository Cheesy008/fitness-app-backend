import pytest
from rest_framework import serializers

from apps.users.serializers import (
    LoginSerializer,
    PasswordResetConfirmSerializer,
    RegistrationSerializer,
    VerifyUserEmailSerializer,
    ResendVerificationLinkSerializer,
    RequestResetPasswordSerializer,
    SetNewPasswordSerializer,
)


pytestmark = [pytest.mark.unit, pytest.mark.django_db]


class TestRegistrationSerializer:
    def test_serialize_model(self):
        payload = {
            "email": "test@gmail.com",
            "password": "password",
            "password_confirmation": "password",
        }
        serializer = RegistrationSerializer(payload)

        assert serializer

    @pytest.mark.parametrize(
        "email, password, password_confirmation, phone_number, validity",
        [
            ("test@gmail.com", "password", "password", "+77074759672", True),
            (
                "test@gmail.com",
                "password",
                "password123",
                "+77074759672",
                False,
            ),  # не совпадают пароли
            (
                "test@gmail.com",
                "password",
                "password123",
                "+7707475961",
                False,
            ),  # неверный формат телефона
        ],
    )
    def test_serialized_data(
        self, email, password, password_confirmation, phone_number, validity
    ):
        payload = {
            "email": email,
            "password": password,
            "password_confirmation": password_confirmation,
            "phone_number": phone_number,
        }
        serializer = RegistrationSerializer(data=payload)

        assert serializer.is_valid() is validity


class TestVerifyUserEmailSerializer:
    def test_serialize_model(self):
        payload = {
            "token": "asdsadassd123"
        }
        serializer = VerifyUserEmailSerializer(payload)

        assert serializer

    def test_serialized_data(self):
        payload = {"token": "asdsadassd123"}
        serializer = VerifyUserEmailSerializer(data=payload)

        assert serializer.is_valid()


class TestResendVerificationLinkSerializer:
    def test_serialize_model(self):
        payload = {
            "email": "test@localhost.com",
        }
        serializer = ResendVerificationLinkSerializer(payload)

        assert serializer

    def test_valid_serialized_data(self, user_factory):
        user_factory.create(email="test@localhost.com", is_verified=False)
        payload = {
            "email": "test@localhost.com",
        }
        serializer = ResendVerificationLinkSerializer(data=payload)
        assert serializer.is_valid()


class TestRequestResetPasswordSerializer:
    def test_serialize_model(self):
        payload = {
            "email": "test@localhost.com",
            "redirect_url": "google.com",
        }
        serializer = RequestResetPasswordSerializer(payload)

        assert serializer

    def test_valid_serialized_data(self):
        payload = {
            "email": "test@localhost.com",
            "redirect_url": "google.com",
        }
        serializer = RequestResetPasswordSerializer(data=payload)

        assert serializer.is_valid()


class TestPasswordResetConfirmSerializer:
    def test_serialize_model(self):
        payload = {
            "token": "asddas23432",
            "uidb64": "QS",
        }
        serializer = PasswordResetConfirmSerializer(payload)

        assert serializer

    def test_valid_serialized_data(self, mocker):
        payload = {
            "token": "asddas23432",
            "uidb64": "QS",
        }
        mocker.patch("apps.users.serializers.auth.check_reset_password_token")

        serializer = PasswordResetConfirmSerializer(data=payload)

        assert serializer.is_valid()


class TestSetNewPasswordSerializer:
    def test_serialize_model(self):
        payload = {
            "password": "password",
            "password_confirmation": "password",
            "token": "asddas23432",
            "uidb64": "QS",
        }
        serializer = SetNewPasswordSerializer(payload)

        assert serializer

    def test_valid_serialized_data(self):
        payload = {
            "password": "password",
            "password_confirmation": "password",
            "token": "asddas23432",
            "uidb64": "QS",
        }
        serializer = SetNewPasswordSerializer(data=payload)

        assert serializer.is_valid()


class TestLoginSerializer:
    def test_serialize_model(self, test_user):
        payload = {"email": test_user.email, "password": "password"}

        serializer = LoginSerializer(payload)

        assert serializer

    @pytest.mark.parametrize(
        "password, validity",
        [
            ("password", True),
            ("incorrect_pass", False),  # некорректный пароль
        ],
    )
    def test_serialized_data(self, test_user, password, validity):
        payload = {"email": test_user.email, "password": password}

        serializer = LoginSerializer(data=payload)

        assert serializer.is_valid() is validity
