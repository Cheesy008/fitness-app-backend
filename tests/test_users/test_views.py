import pytest

from rest_framework.reverse import reverse
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.exceptions import ValidationError


pytestmark = [pytest.mark.unit, pytest.mark.django_db]


# region AuthEndpoints
@pytest.mark.parametrize(
    "email, password, password_confirmation, phone_number, status_code",
    [
        ("test1@gmail.com", "password", "password", "+77074759672", 200),
        (
            "existinguser@gmail.com",
            "password",
            "password123",
            "+7707475961",
            400,
        ),  # пользователь с таким емайлом существует
    ],
)
def test_register_endpoint(
    mocker,
    api_client,
    user_factory,
    email,
    password,
    password_confirmation,
    phone_number,
    status_code,
):
    user_factory.create(email="existinguser@gmail.com")

    endpoint = reverse("auth-register")
    payload = {
        "email": email,
        "password": password,
        "password_confirmation": password_confirmation,
        "phone_number": phone_number,
    }
    mocker.patch("apps.users.services.send_email_task")

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


def test_verify_user_email_endpoint(mocker, api_client, user_factory):
    user_factory.create(email="existinguser@gmail.com")

    endpoint = reverse("auth-verify-email")
    payload = {"token": "fghjkdfghjkfghjk43fdwedf"}
    mocker.patch("apps.users.serializers.auth.verify_user_email")

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == 200


@pytest.mark.parametrize(
    "email, status_code",
    [
        ("existinguser@gmail.com", 200),
        ("unknown@gmail.com", 400),  # пользователя с таким емайлом не существует
    ],
)
def test_request_reset_password_endpoint(
    mocker,
    api_client,
    user_factory,
    email,
    status_code,
):
    endpoint = reverse("auth-request-reset-password")
    payload = {"email": email}
    mocker.patch("apps.users.services.send_email_task")

    user_factory.create(email="existinguser@gmail.com")
    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


@pytest.mark.parametrize(
    "uidb64, token, use_side_effect, status_code",
    [
        ("ab", "hgfdgh", False, 200),
        ("ab", "xvbvcbcvb", True, 400),  # Некорректный токен
    ],
)
def test_password_reset_confirm_endpoint(
    mocker, api_client, uidb64, token, use_side_effect, status_code
):
    payload = {"uidb64": uidb64, "token": token}
    endpoint = reverse("auth-password-reset-confirm")

    if use_side_effect:
        mocker.patch(
            "apps.users.serializers.auth.check_reset_password_token",
            side_effect=ValidationError("error"),
        )
    else:
        mocker.patch(
            "apps.users.serializers.auth.check_reset_password_token",
            return_value=payload,
        )

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


@pytest.mark.parametrize(
    "password, password_confirmation, uidb64, token, use_side_effect, status_code",
    [
        ("password", "password", "ab", "ffdfgfd", False, 200),
        (
            "password",
            "password123",
            "ab",
            "ffdfgfd",
            False,
            400,
        ),  # Пароли не совпадают
        (
            "password",
            "password123",
            "ab",
            "ffdfgfd23432",
            False,
            400,
        ),  # Некорректный токен
    ],
)
def test_set_new_password_endpoint(
    mocker,
    api_client,
    password,
    password_confirmation,
    uidb64,
    token,
    use_side_effect,
    status_code,
):
    payload = {
        "password": password,
        "password_confirmation": password_confirmation,
        "uidb64": uidb64,
        "token": token,
    }
    endpoint = reverse("auth-set-new-password")

    if use_side_effect:
        mocker.patch(
            "apps.users.serializers.auth.set_new_password",
            side_effect=ValidationError("error"),
        )
    else:
        mocker.patch("apps.users.serializers.auth.set_new_password")

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


@pytest.mark.parametrize(
    "email, password, is_active, is_verified, status_code",
    [
        ("existinguser@gmail.com", "password", True, True, 200),
        (
            "existinguser@gmail.com",
            "incorrectpass",
            True,
            True,
            400,
        ),  # некорректный пароль
        (
            "existinguser@gmail.com",
            "incorrectpass",
            True,
            False,
            400,
        ),  # неверифицированный пользователь
        (
            "existinguser@gmail.com",
            "incorrectpass",
            False,
            True,
            400,
        ),  # неактивный пользователь
        (
            "unknown@gmail.com",
            "password",
            False,
            False,
            400,
        ),  # пользователя с таким емайлом не существует
    ],
)
def test_login_endpoint(
    api_client,
    user_factory,
    email,
    password,
    is_active,
    is_verified,
    status_code,
):
    user_factory.create(
        email="existinguser@gmail.com", is_active=is_active, is_verified=is_verified
    )

    endpoint = reverse("auth-login")
    payload = {"email": email, "password": password}

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


# endregion


# region UserEndpoints
def test_current_user_endpoint(api_client_with_credentials):
    endpoint = reverse("user-current-user")

    response = api_client_with_credentials.get(endpoint)
    assert response.status_code == 200


def test_current_user_is_not_verified(custom_api_client_with_credentials, user_factory):
    endpoint = reverse("user-current-user")
    user = user_factory.create(is_verified=False)

    response = custom_api_client_with_credentials(user).get(endpoint)
    assert response.status_code == 403


@pytest.mark.parametrize(
    "field",
    [
        ("email"),
        ("first_name"),
        ("last_name"),
        ("phone_number"),
        ("birth_date"),
    ],
)
def test_edit_current_user_endpoint(api_client_with_credentials, field):
    endpoint = reverse("user-edit-current-user")
    payload = {
        "email": "test@gmail.com",
        "first_name": "test",
        "last_name": "testov",
        "phone_number": "+77074759621",
        "birth_date": "2000-10-02",
    }
    valid_field = payload[field]

    response = api_client_with_credentials.post(
        endpoint, data={field: valid_field}, format="json"
    )
    assert response.status_code == 200
    assert response.data[field] == payload[field]


@pytest.mark.parametrize(
    "file, content_type, status_code",
    [
        ("test.png", "image/png", 200),
        ("test.jpg", "image/jpg", 200),
        ("test.txt", "text/plain", 400),
    ],
)
def test_upload_avatar_endpoint(
    api_client_with_credentials, file, content_type, status_code
):
    endpoint = reverse("upload-user-avatar")
    image = SimpleUploadedFile(file, b"file_content", content_type=content_type)

    response = api_client_with_credentials.post(endpoint, data={"image": image})
    assert response.status_code == status_code


# endregion
