import pytest

from django.utils.encoding import smart_bytes
from django.utils.http import urlsafe_base64_encode

from apps.users.services import decode_and_find_user


pytestmark = [pytest.mark.unit, pytest.mark.django_db]


def test_decode_and_find_user_valid(user_factory):
    user = user_factory.create()
    uidb64 = urlsafe_base64_encode(smart_bytes(user.id))

    assert decode_and_find_user(uidb64) == user


def test_decode_and_find_user_invalid_uidb64():
    with pytest.raises(Exception) as e:
        decode_and_find_user("blabla")
        
    assert "Invalid uidb64" == e.value.args[0]
    