from datetime import date
import random
import factory
from factory import fuzzy
from faker import Factory as FakerFactory

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.db.models.signals import post_save

from apps.users.models import Profile

User = get_user_model()

faker = FakerFactory.create()


class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile

    first_name = faker.first_name()
    last_name = faker.last_name()
    birth_date = fuzzy.FuzzyDate(date(1990, 1, 1))
    user = factory.SubFactory("tests.test_users.factories.UserFactory")


@factory.django.mute_signals(post_save)
class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Sequence(lambda n: "user{}@example.com".format(n))
    password = factory.LazyFunction(lambda: make_password("password"))
    phone_number = factory.Sequence(lambda n: f"+77074{random.randint(1, 9)}{random.randint(1, 9)}{random.randint(1, 9)}67{random.randint(1, 9)}")
    is_verified = True
    profile = factory.RelatedFactory(ProfileFactory, factory_related_name="user")
