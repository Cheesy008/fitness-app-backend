from datetime import date
from dateutil.relativedelta import relativedelta

import pytest

from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model


User = get_user_model()

pytestmark = [pytest.mark.unit, pytest.mark.django_db]


def test_user_create_count(user_factory):
    size = 5
    user_factory.create_batch(size)

    assert User.objects.count() == size


def test_user_phone_number_invalid(user_factory):
    with pytest.raises(ValidationError):
        user_factory.create(phone_number="+770747596731")


def test_user_phone_number_not_unique(user_factory):
    with pytest.raises(ValidationError):
        user_factory.create(phone_number="+770747596731")
        user_factory.create(phone_number="+770747596731")


def test_user_age(user_factory):
    birth_date = date(2000, 10, 2)

    user = user_factory.create()
    user.profile.birth_date = birth_date
    user.save()

    age = relativedelta(date.today(), birth_date).years

    assert user.profile.age == age


def test_user_change_password(user_factory):
    user = user_factory.create()
    user.change_password("new_pass")

    assert user.check_password("new_pass")
