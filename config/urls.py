from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from apps.core.urls import urlpatterns as core_urlpatterns
from apps.users.urls import urlpatterns as user_urlpatterns
from apps.catalogs.urls import urlpatterns as catalogs_urlpatterns
from apps.competitions.urls import urlpatterns as competitions_urlpatterns

api_urlpatterns = [
    *user_urlpatterns,
    *core_urlpatterns,
    *catalogs_urlpatterns,
    *competitions_urlpatterns,
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(api_urlpatterns)),
    path('api/docs/', include('docs.urls')),
]


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if not settings.USE_S3:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

    