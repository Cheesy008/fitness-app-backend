import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible


@deconstructible
class PathUploader:
    """
    Класс для создания кастомного пути до медиафайлов.
    """

    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split(".")[-1]

        if instance.pk:
            filename = "{}.{}".format(instance.pk, ext)
        else:
            filename = "{}.{}".format(uuid4().hex, ext)

        return os.path.join(self.sub_path, filename)


PROFILE_AVATAR_UPLOAD_PATH = PathUploader("users/avatars/")
DOCUMENT_PHOTO_UPLOAD_PATH = PathUploader("users/document-photo/")
GALLERY_IMAGE_UPLOAD_PATH = PathUploader("catalogs/gallery/")
BRAND_LOGO_UPLOAD_PATH = PathUploader("catalogs/brand/")
COMPETITION_IMAGE_UPLOAD_PATH = PathUploader("competition/image/")
