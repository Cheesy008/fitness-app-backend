import os
from typing import Union

import phonenumbers
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


_3MB = 3145728  # 3 мегабайта в байтах


def validate_phone_number(phone_number: str) -> Union[bool, str]:
    try:
        phone_number = phonenumbers.parse(phone_number, None)
    except:
        return False

    if not phonenumbers.is_possible_number(
        phone_number
    ) or not phonenumbers.is_valid_number(phone_number):
        return False

    return phonenumbers.format_number(phone_number, phonenumbers.PhoneNumberFormat.E164)


def validate_image_extension(filename: str):
    valid_file_extensions = [".jpeg", ".jpg", ".png", ".svg"]
    ext = os.path.splitext(filename)[1]
    if ext.lower() not in valid_file_extensions:
        raise ValidationError(_("Unacceptable file extension."))


def validate_image_size(size):
    if size > _3MB:
        raise ValidationError(_("The maximum file size that can be uploaded is 3MB"))


def validate_image(image):
    validate_image_extension(image.name)
    validate_image_size(image.size)
