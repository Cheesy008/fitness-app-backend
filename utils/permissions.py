from rest_framework.permissions import BasePermission
from django.utils.translation import ugettext_lazy as _


class IsVerified(BasePermission):
    """
    Пермишн на проверку верификации пользователя.
    """
    message = _("User is not verified")
    
    def has_permission(self, request, view):
        return request.user.is_verified
