from django.db.models import IntegerChoices


class TicketType(IntegerChoices):
    """
    Типы билета (обычный, ВИП).
    """

    REGULAR = 1
    VIP = 2

