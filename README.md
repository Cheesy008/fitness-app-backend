# fintess.app-backend

## Подготовока окружения для разработки
Создать в папке `env` файл `env.dev`. 
Пример переменных окружения смотреть в `.env.sample`.

## Запуск
#### Запуск контейнеров:
```bash
# Запуск из docker compose
docker-compose -f dev.yml up
# Для того, чтобы зайти в django контейнер
docker-compose -f dev.yml exec backend bash
# Перед первым запуском необходимо прописать следующие команды в контейнере
python3 manage.py migrate
# Если надо создать суперюзера
python3 manage.py superuser admin@localhost.com password
```
