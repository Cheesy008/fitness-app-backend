from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import viewsets, filters

from django_filters.rest_framework import DjangoFilterBackend


@api_view()
def health_check(request):
    """
    Эндпоинт для проверки того, что сервер работает.
    """
    return Response(status=200)


class AppModelBase:
    """
    Базовый вьюсет, от которого должны наследоваться AppModelViewSet
    и AppModelReadOnlyViewSet.

    Здесь определена базовая фильтрация и выбор сериалайзера, в
    зависимости от типа запроса.
    """

    filter_backends = [
        filters.SearchFilter,
        DjangoFilterBackend,
        filters.OrderingFilter,
    ]
    list_serializer_class = None
    create_update_serializer_class = None
    create_serializer_class = None
    update_serializer_class = None

    def get_serializer_class(self):
        if self.action == "list" and self.list_serializer_class:
            return self.list_serializer_class
        if self.action == "create":
            if self.create_serializer_class:
                return self.create_serializer_class
            if self.create_update_serializer_class:
                return self.create_update_serializer_class
        if self.action == "update":
            if self.update_serializer_class:
                return self.update_serializer_class
            if self.create_update_serializer_class:
                return self.create_update_serializer_class
        return self.serializer_class


class AppModelViewSet(AppModelBase, viewsets.ModelViewSet):
    """
    Базовый вьюсет, который подразумевает под собой CRUD операции
    и от которого должны наследоваться остальные вьюсеты.
    """

    pass


class AppModelReadOnlyViewSet(AppModelBase, viewsets.ReadOnlyModelViewSet):
    """
    Базовый вьюсет, который подразумевает под собой Read only операции
    и от которого должны наследоваться остальные вьюсеты.
    """

    pass
