from django.db import models
from django.utils.translation import ugettext_lazy as _


class BaseModel(models.Model):
    """
    Абстрактная модель, в которой определены базовые поля и от
    которой должны наследоваться остальные модели.
    """
    
    id = models.BigAutoField(_("ID"), primary_key=True, editable=False)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True, null=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True, null=True)

    class Meta:
        abstract = True
