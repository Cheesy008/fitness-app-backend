from apps.core.views import AppModelReadOnlyViewSet
from .serializers import (
    GalleryCategoryListSerializer,
    GalleryCategoryDetailSerializer,
    SponsorSerializer,
)
from .models import GalleryCategory, Sponsor


class GalleryViewSet(AppModelReadOnlyViewSet):
    """
    Получение категорий и изображений.
    """

    queryset = GalleryCategory.objects.all()
    list_serializer_class = GalleryCategoryListSerializer
    serializer_class = GalleryCategoryDetailSerializer


class SponsorViewSet(AppModelReadOnlyViewSet):
    """
    Получение спонсоров.
    """

    queryset = Sponsor.objects.all()
    serializer_class = SponsorSerializer
