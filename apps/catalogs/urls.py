from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import GalleryViewSet, SponsorViewSet

router = DefaultRouter()

router.register(r"gallery", GalleryViewSet, "gallery")
router.register(r"sponsor", SponsorViewSet, "sponsor")

urlpatterns = [
    path("", include(router.urls)),
]
