from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.core.models import BaseModel
from utils.media_path import GALLERY_IMAGE_UPLOAD_PATH, BRAND_LOGO_UPLOAD_PATH
from utils.validators import validate_image
from .managers import GalleryCategoryManager


# region Gallery
class GalleryImage(BaseModel):
    """
    Модель изображения из галереи.
    """
    image = models.ImageField(
        _("Gallery image"),
        upload_to=GALLERY_IMAGE_UPLOAD_PATH,
        null=True,
        blank=True,
    )
    gallery_category = models.ForeignKey(
        "GalleryCategory",
        on_delete=models.CASCADE,
        related_name="images",
        verbose_name=_("Category"),
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _("Gallery image")
        verbose_name_plural = _("Gallery images")

    def __str__(self) -> str:
        return f"Gallery image #{self.id}"

    def clean(self) -> None:
        if self.image:
            validate_image(self.image)
        return super().clean()

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save()


class GalleryCategory(BaseModel):
    """
    Модель категории из галереи.
    """
    name = models.CharField(_("Category name"), max_length=150, null=True, blank=True)
    
    objects = GalleryCategoryManager()
    
    class Meta:
        verbose_name = _("Gallery category")
        verbose_name_plural = _("Gallery categories")
        ordering = ['name']
    
    def __str__(self) -> str:
        return self.name


# endregion


class Sponsor(BaseModel):
    """
    Модель спонсора.
    """
    name = models.CharField(_("Brand name"), max_length=150, null= True, blank=True)
    logo = models.ImageField(
        _("Logo"),
        upload_to=BRAND_LOGO_UPLOAD_PATH,
        null=True,
        blank=True,
    )
    description = models.TextField(_("Description"), null=True, blank=True)
        
    class Meta:
        verbose_name = _("Sponsor")
        verbose_name_plural = _("Sponsors")
        ordering = ["name"]
