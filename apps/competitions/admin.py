from django.contrib import admin

from .models import CompetitionCategory, Competition, CompetitionImage, CompetitionRegistration, ViewerTicket


admin.site.register(CompetitionRegistration)
admin.site.register(ViewerTicket)

@admin.register(Competition)
class CompetitionAdmin(admin.ModelAdmin):
    class CompetitionImageTabularInline(admin.TabularInline):
        model = CompetitionImage
        extra = 0

    class CompetitionCategoryTabularInline(admin.TabularInline):
        model = CompetitionCategory
        extra = 0

    inlines = [CompetitionImageTabularInline, CompetitionCategoryTabularInline]
