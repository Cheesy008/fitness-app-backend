from django.db.models import Manager


class CompetitionManager(Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .prefetch_related("sponsors", "competition_categories")
        )
