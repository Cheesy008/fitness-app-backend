from rest_framework import views

from apps.competitions.models import Competition
from apps.core.views import AppModelReadOnlyViewSet
from .serializers import CompetitionDetailSerializer, CompetitionListSerializer


class CompetitionViewSet(AppModelReadOnlyViewSet):
    """
    Вьюсет для получения списка/конкретного экземпляра соревнований.
    """

    queryset = Competition.objects.all()
    list_serializer_class = CompetitionListSerializer
    serializer_class = CompetitionDetailSerializer


class BuyTicketAPIView(views.APIView):
    """
    API для покупки билетов зрителей.
    """

    def post(self, request):
        pass


class CheckInCompetitorAPIView(views.APIView):
    """
    API для записи участника на категорию.
    """
    
    def post(self, request):
        pass
