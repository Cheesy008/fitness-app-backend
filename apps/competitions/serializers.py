from rest_framework import serializers

from .models import Competition, CompetitionImage, CompetitionCategory, ViewerTicket
from apps.catalogs.serializers import SponsorSerializer


class CompetitionImageSerializer(serializers.ModelSerializer):
    """
    Сериалайзер изображений соревнования.
    """

    class Meta:
        model = CompetitionImage
        fields = ("id", "image")


class CompetitionCategorySerializer(serializers.ModelSerializer):
    """
    Сериалайзер категории.
    """

    class Meta:
        model = CompetitionCategory
        fields = ("id", "name", "price")


class CompetitionListSerializer(serializers.ModelSerializer):
    """
    Сериалайзер для вывода списка соревнований.
    """

    class Meta:
        model = Competition
        fields = ("id", "name", "date", "location")


class CompetitionDetailSerializer(serializers.ModelSerializer):
    """
    Сериалайзер для вывода детального описания соревнования.
    """

    images = CompetitionImageSerializer(many=True)
    competition_categories = CompetitionCategorySerializer(many=True)
    sponsors = SponsorSerializer(many=True)

    class Meta:
        model = Competition
        fields = (
            "id",
            "name",
            "date",
            "description",
            "location",
            "vip_ticket_price",
            "regular_ticket_price",
            "images",
            "competition_categories",
            "sponsors",
        )


class ViewerTicketSerializer(serializers.ModelSerializer):
    competition_name = serializers.CharField(source="competition.name")
    competition_date = serializers.CharField(source="competition.date")
    competition_location = serializers.CharField(source="competition.location")

    class Meta:
        model = ViewerTicket
        fields = (
            "id",
            "ticket_type",
            "competition_name",
            "competition_date",
            "competition_location",
        )
        read_only_fields = ("__all__",)


class CompetitionUserInfoSerializer(serializers.ModelSerializer):
    """
    Сериалайзер для вывода данных о соревнованиях и категориях
    для пользователя, который записался в качестве участника.
    """

    competition_categories = CompetitionCategorySerializer(many=True)

    class Meta:
        model = Competition
        fields = (
            "id",
            "name",
            "date",
            "description",
            "location",
            "competition_categories",
        )
