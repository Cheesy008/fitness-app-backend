from apps.core.models import BaseModel
from datetime import date
from dateutil.relativedelta import relativedelta

from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from rest_framework_simplejwt.tokens import RefreshToken

from .managers import CustomUserManager
from utils.media_path import DOCUMENT_PHOTO_UPLOAD_PATH, PROFILE_AVATAR_UPLOAD_PATH
from utils.validators import (
    validate_phone_number,
    validate_image,
)


class User(AbstractBaseUser, PermissionsMixin):
    username = None
    email = models.EmailField(_("Email"), unique=True, null=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    is_verified = models.BooleanField(_("Is verified"), default=False)
    phone_number = models.CharField(
        _("Phone number"), max_length=12, null=True, blank=True, unique=True
    )

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def __str__(self):
        return self.email

    def change_password(self, password):
        self.set_password(password)
        self.save()

    def get_tokens_for_user(self):
        refresh = RefreshToken.for_user(self)
        return str(refresh), str(refresh.access_token)

    def clean(self) -> None:
        if self.phone_number is not None and not validate_phone_number(
            self.phone_number
        ):
            raise ValidationError(_("Incorrect phone number"))

        return super().clean()

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save()


class Profile(models.Model):
    """
    Модель профиля, в которой собрана дополнительная необязательная
    информация пользователя.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    first_name = models.CharField(_("First name"), max_length=30, null=True, blank=True)
    last_name = models.CharField(_("Last name"), max_length=30, null=True, blank=True)
    birth_date = models.DateField(_("Date of birth"), null=True, blank=True)
    avatar = models.ImageField(
        _("Avatar"),
        upload_to=PROFILE_AVATAR_UPLOAD_PATH,
        null=True,
        blank=True,
    )
    uae_id = models.CharField(max_length=100, null=True, blank=True)
    passport_id = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")

    def __str__(self):
        return self.user.email

    @property
    def full_name(self):
        return (
            f"{self.first_name} {self.last_name}"
            if self.first_name and self.last_name
            else ""
        )

    @property
    def age(self):
        return relativedelta(date.today(), self.birth_date).years

    def clean(self) -> None:
        if self.avatar:
            validate_image(self.avatar)

        return super().clean()

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save()


class UserDocumentPhoto(BaseModel):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="document_photos",
        verbose_name=_("User"),
    )
    image = models.ImageField(
        _("Document photo"),
        upload_to=DOCUMENT_PHOTO_UPLOAD_PATH,
        null=True,
    )

    class Meta:
        verbose_name = _("Document photo")
        verbose_name_plural = _("Document photos")

    def clean(self) -> None:
        if self.user.document_photos.count() >= 3:
            raise ValidationError(_("User can't have more than 3 document photos"))

        return super().clean()

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save()
