from django.contrib.auth import authenticate, get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.tokens import RefreshToken, TokenError

from apps.users.services import (
    check_reset_password_token,
    send_activation_link,
    send_reset_password_link,
    set_new_password,
    update_user_password,
    verify_user_email,
)


User = get_user_model()


class UserValidateVerificationMixin:
    """
    Микскин для проверки, является ли пользователь
    верифицированным.
    """

    def validate(self, attrs):
        email = attrs.get("email")

        user = User.objects.find_by_email(email)
        if user and user.is_verified:
            raise ValidationError(_("User has already been verified"))

        attrs.update({"user": user})

        return attrs


class RegistrationSerializer(UserValidateVerificationMixin, serializers.Serializer):
    """
    Сериалайзер для регистрации.
    """

    email = serializers.EmailField(max_length=255, required=True)
    password = serializers.CharField(
        min_length=6,
        max_length=68,
        style={"input_type": "password"},
        required=True,
        write_only=True,
    )
    password_confirmation = serializers.CharField(
        min_length=6,
        max_length=68,
        style={"input_type": "password"},
        required=True,
        write_only=True,
    )
    phone_number = serializers.CharField(min_length=12, max_length=12, required=True)
    redirect_url = serializers.CharField(required=False)

    class Meta:
        fields = (
            "email",
            "password",
            "password_confirmation",
            "phone_number",
            "redirect_url",
        )

    def validate(self, attrs):
        attrs = super().validate(attrs)

        if attrs["password"] != attrs["password_confirmation"]:
            raise ValidationError("Passwords didn't match")

        attrs.pop("password_confirmation")

        return attrs

    def create(self, validated_data):
        user = validated_data.pop("user", None)
        redirect_url = validated_data.pop("redirect_url", "")

        # Если пользователя не существует, то записываем его в бд.
        if user is None:
            user = User.objects.create_user(**validated_data)

        send_activation_link(
            user=user,
            redirect_url=redirect_url,
        )

        return user


class VerifyUserEmailSerializer(serializers.Serializer):
    """
    Сериалайзер для подтверждения емайла пользователя.
    """

    token = serializers.CharField()

    def save(self, **kwargs):
        verify_user_email(token=self.validated_data["token"])


class ResendVerificationLinkSerializer(
    UserValidateVerificationMixin, serializers.Serializer
):
    """
    Сериалайзер для отправки повторной ссылки на подтверждение.
    """

    email = serializers.EmailField()
    redirect_url = serializers.CharField(required=False)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if attrs.get("user") is None:
            raise ValidationError(_("User does not exist"))
        return attrs

    def save(self, **kwargs):
        send_activation_link(
            user=self.validated_data["user"],
            redirect_url=self.validated_data.get("redirect_url", ""),
        )


class RequestResetPasswordSerializer(serializers.Serializer):
    """
    Сериалайзер для отправки ссылки на сброс пароля.
    """

    email = serializers.EmailField()
    redirect_url = serializers.CharField(required=False)

    def save(self, **kwargs):
        send_reset_password_link(
            email=self.validated_data["email"],
            redirect_url=self.validated_data.get("redirect_url", ""),
        )


class PasswordResetConfirmSerializer(serializers.Serializer):
    """
    Сериалайзер для валидации строк, полученных из ссылки
    на сброс пароля.
    """

    uidb64 = serializers.CharField()
    token = serializers.CharField()

    def validate(self, attrs):
        check_reset_password_token(
            uidb64=attrs["uidb64"],
            token=attrs["token"],
        )

        return attrs


class SetNewPasswordSerializer(serializers.Serializer):
    """
    Сериалайзер для установки нового пароля пользователю.
    """

    password = serializers.CharField(min_length=6, max_length=68, write_only=True)
    password_confirmation = serializers.CharField(
        style={"input_type": "password"}, min_length=6, max_length=68, write_only=True
    )
    token = serializers.CharField(min_length=1, write_only=True)
    uidb64 = serializers.CharField(min_length=1, write_only=True)

    def validate(self, attrs):
        if attrs["password"] != attrs["password_confirmation"]:
            raise ValidationError(_("Passwords didn't match"))

        return attrs

    def save(self, **kwargs):
        set_new_password(
            password=self.validated_data["password"],
            token=self.validated_data["token"],
            uidb64=self.validated_data["uidb64"],
        )


class UpdatePasswordSerializer(serializers.Serializer):
    """
    Сериалайзер для установки нового пароля пользователю.
    
    В данном случае пароль устанавливает сам пользователь.
    """

    password = serializers.CharField(min_length=6, max_length=68, write_only=True)
    password_confirmation = serializers.CharField(
        style={"input_type": "password"}, min_length=6, max_length=68, write_only=True
    )

    def validate(self, attrs):
        if attrs["password"] != attrs["password_confirmation"]:
            raise ValidationError(_("Passwords didn't match"))

        return attrs

    def save(self, **kwargs):
        update_user_password(
            password=self.validated_data["password"],
            user=self.context['user']
        )

class LoginSerializer(serializers.Serializer):
    """
    Сериалайзер для входа на сайт.
    """

    email = serializers.EmailField(max_length=255, required=True)
    password = serializers.CharField(
        style={"input_type": "password"},
        min_length=6,
        max_length=68,
        required=True,
        write_only=True,
    )
    tokens = serializers.SerializerMethodField()

    def get_tokens(self, obj):
        user = User.objects.find_by_email(obj["email"])
        refresh, access = user.get_tokens_for_user()
        return {"access": access, "refresh": refresh}

    def validate(self, attrs):
        email = attrs.get("email")
        password = attrs.get("password")

        user = authenticate(email=email, password=password)
        if not user:
            raise ValidationError(_("Incorrect data"))
        if not user.is_active:
            raise ValidationError(_("Account is blocked"))
        if not user.is_verified:
            raise ValidationError(_("Email is not verified"))

        return attrs


class LogoutSerializer(serializers.Serializer):
    """
    Сериалайзер для выхода из сайта.

    При выходе мы получаем refresh токен, который
    должны добавить в черный список, для того, чтобы
    в последствии его никто не смог переиспользовать.
    """

    refresh = serializers.CharField()

    def save(self, **kwargs):
        try:
            RefreshToken(self.validated_data["refresh"]).blacklist()
        except TokenError:
            raise ValidationError(_("Token is invalid or expired"))
