from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from apps.users.services import update_user_profile
from apps.users.models import UserDocumentPhoto
from apps.competitions.models import CompetitionRegistration, ViewerTicket


User = get_user_model()


class UserDocumentPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDocumentPhoto
        fields = ("id", "image")
        read_only_fields = ("__all__",)


class UserSerializer(serializers.ModelSerializer):
    """
    Сериалайзер для вывода и редактирования данных пользователя.
    """

    first_name = serializers.CharField(
        source="profile.first_name",
        required=False,
        allow_null=True,
    )
    last_name = serializers.CharField(
        source="profile.last_name",
        required=False,
        allow_null=True,
    )
    avatar = serializers.ImageField(
        source="profile.avatar",
        required=False,
        allow_null=True,
    )
    document_photos = UserDocumentPhotoSerializer(many=True, read_only=True)
    birth_date = serializers.DateField(
        source="profile.birth_date",
        required=False,
        allow_null=True,
    )
    age = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "is_verified",
            "is_active",
            "first_name",
            "last_name",
            "avatar",
            "document_photos",
            "phone_number",
            "birth_date",
            "age",
        )
        extra_kwargs = {
            "is_verified": {"read_only": True},
            "is_active": {"read_only": True},
        }

    def get_age(self, instance):
        return instance.profile.age or None

    def update(self, instance, validated_data):
        updated_instance = update_user_profile(instance=instance, data=validated_data)
        return updated_instance


class UploadUsePhotoSerializer(serializers.Serializer):
    image = serializers.ImageField()


class ResponseUploadUsePhotoSerializer(serializers.Serializer):
    image_url = serializers.CharField()
