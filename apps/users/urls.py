from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenRefreshView

from .views import (
    AuthViewSet,
    UserViewset,
    UploadUserAvatarView,
    UserDocumentPhotoViewSet,
)

router = DefaultRouter()

router.register(r"auth", AuthViewSet, "auth")
router.register(r"user", UserViewset, "user")

urlpatterns = [
    path("", include(router.urls)),
    path(
        "user/me/upload-document-photo",
        UserDocumentPhotoViewSet.as_view({"post": "upload_document_photo"}),
        name="upload-document-photo",
    ),
    path(
        "user/me/delete-document-photo/<pk>",
        UserDocumentPhotoViewSet.as_view({"delete": "delete_document_photo"}),
        name="photo-document-photo",
    ),
    path("user/me/upload-avatar", UploadUserAvatarView.as_view(), name="upload-user-avatar"),
    path("auth/login/refresh/", TokenRefreshView.as_view(), name="login_refresh"),
]
