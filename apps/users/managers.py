from django.contrib.auth.models import BaseUserManager


class CustomUserManager(BaseUserManager):
    """
    Переопределенный BaseUserManager для возможности логиниться и
    регистрироваться посредством емайла, а не никнейма.
    """

    def get_queryset(self):
        return super().get_queryset().select_related("profile")

    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError("Email field is required")

        email = self.normalize_email(email)
        user = self.model(email=email, **kwargs)
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault("is_verified", True)

        return self.create_user(email, password, **extra_fields)

    def find_by_email(self, email):
        return self.get_queryset().filter(email=email).first()

    def find_by_id(self, id):
        return self.get_queryset().filter(id=id).first()
