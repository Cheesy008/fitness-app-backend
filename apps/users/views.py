from rest_framework import status, views, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ParseError
from rest_framework.parsers import MultiPartParser
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from .serializers import (
    LoginSerializer,
    LogoutSerializer,
    PasswordResetConfirmSerializer,
    RegistrationSerializer,
    RequestResetPasswordSerializer,
    ResendVerificationLinkSerializer,
    ResponseUploadUsePhotoSerializer,
    SetNewPasswordSerializer,
    UpdatePasswordSerializer,
    UploadUsePhotoSerializer,
    UserSerializer,
    VerifyUserEmailSerializer,
)
from .models import UserDocumentPhoto
from apps.core.serializers import ApiErrorsMixin
from apps.core.throttles import SendEmailRateThrottle
from apps.users.services import (
    delete_document_photo,
    upload_document_photo,
    upload_profile_avatar,
)
from apps.competitions.models import Competition, ViewerTicket
from utils.permissions import IsVerified
from apps.competitions.serializers import CompetitionDetailSerializer, CompetitionUserInfoSerializer, ViewerTicketSerializer

User = get_user_model()


class AuthViewSet(ApiErrorsMixin, viewsets.ViewSet):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(
        method="post",
        request_body=RegistrationSerializer(),
        responses={200: "Activation link was sent to your email"},
    )
    @action(detail=False, methods=["post"])
    def register(self, request, *args, **kwargs):
        """
        Регистрация и отправка письма о подтверждении на емайл.
        """
        serializer = RegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("Activation link was sent to your email")}, status.HTTP_200_OK
        )

    @swagger_auto_schema(
        method="post",
        request_body=VerifyUserEmailSerializer(),
        responses={200: "Email has been successfully verified"},
    )
    @action(detail=False, methods=["post"], url_path="verify-email")
    def verify_email(self, request, *args, **kwargs):
        """
        Подтверждение емайла пользователя.
        """
        serializer = VerifyUserEmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("Email has been successfully verified")}, status.HTTP_200_OK
        )

    @swagger_auto_schema(
        method="post",
        request_body=ResendVerificationLinkSerializer(),
        responses={200: "The activation link has been sent to your email again"},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="resend-verification-link",
    )
    def resend_verification_link(self, request, *args, **kwargs):
        """
        Повторная отправки ссылки на подтверждение пользователя.
        """
        serializer = ResendVerificationLinkSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("The activation link has been sent to your email again")},
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(
        method="post",
        request_body=RequestResetPasswordSerializer(),
        responses={200: "The link to reset password has been sent to your email"},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="request-reset-password",
        throttle_classes=[SendEmailRateThrottle],
    )
    def request_reset_password(self, request, *args, **kwargs):
        """
        Запрос о сбросе пароля.
        """
        serializer = RequestResetPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("The reset password link has been sent to your email")},
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(
        method="post",
        request_body=PasswordResetConfirmSerializer(),
        responses={200: PasswordResetConfirmSerializer},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="password-reset-confirm",
    )
    def password_reset_confirm(self, request, *args, **kwargs):
        """
        Подтверждение о сбросе пароля.
        """
        serializer = PasswordResetConfirmSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.validated_data, status.HTTP_200_OK)

    @swagger_auto_schema(
        method="post",
        request_body=SetNewPasswordSerializer(),
        responses={200: "The password was updated successfully"},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="set-new-password",
    )
    def set_new_password(self, request, *args, **kwargs):
        """
        Установка нового пароля.
        """
        serializer = SetNewPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("The password was updated successfully")},
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(
        method="post",
        request_body=UpdatePasswordSerializer(),
        responses={200: "The password was updated successfully"},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="update-user-password",
        permission_classes=[IsAuthenticated],
    )
    def update_user_password(self, request, *args, **kwargs):
        """
        Обновление пароля самим пользователем.
        """
        serializer = UpdatePasswordSerializer(
            data=request.data, context={"user": request.user}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("The password was updated successfully")},
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(method="post", request_body=LoginSerializer())
    @action(detail=False, methods=["post"])
    def login(self, request, *args, **kwargs):
        """
        Авторизация на сайт.
        """
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status.HTTP_200_OK)

    @swagger_auto_schema(
        method="post", request_body=LogoutSerializer(), responses={204: "Logout"}
    )
    @action(detail=False, methods=["post"], permission_classes=[IsAuthenticated])
    def logout(self, request, *args, **kwargs):
        """
        Выход с сайта.
        """
        serializer = LogoutSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_204_NO_CONTENT)


class UserViewset(ApiErrorsMixin, viewsets.ViewSet):
    permission_classes = (
        IsAuthenticated,
        IsVerified,
    )

    @swagger_auto_schema(method="get", responses={200: UserSerializer()})
    @action(methods=["get"], detail=False, url_path="me")
    def current_user(self, request, *args, **kwargs):
        """
        Получение данных текущего пользователя.
        """
        return Response(
            UserSerializer(request.user, context={"request": request}).data,
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(
        method="post",
        request_body=UserSerializer(),
        responses={200: UserSerializer},
    )
    @action(methods=["post"], detail=False, url_path="me/edit")
    def edit_current_user(self, request, *args, **kwargs):
        """
        Редактирование данных текущего пользователя.
        """
        instance = request.user
        serializer = UserSerializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        method="get",
        responses={200: ViewerTicketSerializer},
    )
    @action(methods=["get"], detail=False, url_path="me/tickets")
    def get_tickets(self, request, *args, **kwargs):
        """
        Получение билетов на соревнования в качестве зрителя.
        """
        queryset = ViewerTicket.objects.filter(user=request.user)
        serializer = ViewerTicketSerializer(queryset, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
    
    
    @swagger_auto_schema(
        method="get",
        responses={200: CompetitionUserInfoSerializer},
    )
    @action(methods=["get"], detail=False, url_path="me/competition-registrations")
    def get_competition_registrations(self, request, *args, **kwargs):
        """
        Получение записей на соревнование в качестве участника.
        """
        queryset = Competition.objects.filter(competition_categories__competition_registrations__user=request.user)
        serializer = CompetitionUserInfoSerializer(queryset, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class UploadUserAvatarView(ApiErrorsMixin, views.APIView):
    permission_classes = (IsAuthenticated, IsVerified)
    parser_classes = (MultiPartParser,)

    @swagger_auto_schema(
        operation_description="Upload image in the following formats: jpg, jpeg, png, svg",
        manual_parameters=[
            openapi.Parameter(
                name="image",
                in_=openapi.IN_FORM,
                type=openapi.TYPE_FILE,
                required=True,
            )
        ],
    )
    def post(self, request):
        """
        Загрузить аватар текущему пользователю.
        """
        try:
            image = request.data["image"]
            upload_profile_avatar(image=image, user=request.user)
            return Response(status=status.HTTP_200_OK)
        except KeyError:
            raise ParseError(_("Request has no image attached"))


class UserDocumentPhotoViewSet(ApiErrorsMixin, viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated, IsVerified)
    parser_classes = (MultiPartParser,)
    serializer_class = UploadUsePhotoSerializer

    def get_queryset(self, request):
        return UserDocumentPhoto.objects.filter(user=request.user)

    @swagger_auto_schema(
        method="post",
        request_body=UploadUsePhotoSerializer(),
        responses={200: ResponseUploadUsePhotoSerializer},
    )
    @action(methods=["post"], detail=False, url_path="upload-document-photo")
    def upload_document_photo(self, request, *args, **kwargs):
        """
        Загрузка фото для документа.
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        image = upload_document_photo(
            user=request.user, image=serializer.validated_data["image"]
        )
        response_serializer = ResponseUploadUsePhotoSerializer(
            instance={"image_url": request.build_absolute_uri(image)}
        )
        return Response(response_serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(method="delete")
    @action(methods=["delete"], detail=True, url_path="delete-document-photo")
    def delete_document_photo(self, request, pk, *args, **kwargs):
        """
        Удаление фото для документа.
        """
        delete_document_photo(id=pk)
        return Response(status=status.HTTP_204_NO_CONTENT)
