#!/bin/bash

sudo docker build -t cheesy008/fitness.app-backend:prod -f Dockerfiles/Dockerfile.prod .
sudo docker build -t cheesy008/fitness.app-nginx:latest -f Dockerfiles/Dockerfile.nginx .

docker push cheesy008/fitness.app-backend:prod
docker push cheesy008/fitness.app-nginx:latest
